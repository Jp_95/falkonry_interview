#Problem 1

def find_missing():
	"""Function to find a missing number in unsorted array of numbers between 1 to 10"""
	nums = [] 
	check_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
	while (len(nums)!=9):
		num = input("Enter the Number : ")
		try:
			if int(num) > 0 and int(num) < 11:
				if int(num) in check_list:
					nums.append(int(num))
					check_list.remove(int(num))
					print('Number Pushed to array:',num,"\nCurrent Array Structure(unsorted array):",nums)
				else:	
					# print("Enter another element:\t")	
					print("The number is already in the array")
			else:
				print("Please enter a number between 1-10")
		except(ValueError):
			print('Please Enter a Valid Number')
			continue

	print('Missing Number ->',check_list[0])									#without using Sets
	# print("Missing Number :",list(set(check_list).difference(set(nums)))[0])	#using Sets
	return check_list[0]

find_missing()
