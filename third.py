#Problem 3 -> Solution 3.1

def array_input():
	"""Function for feeding input array"""	
	nums = input("Enter the numbers of the series seperated by <space>\n")
	nums = nums.split()
	try:
		nums = [int(num) for num in nums]
		# elems=[]
		# for i in range(0,len(nums),10):
		# 	elems.append(nums[i:i+10])
		elements = [nums[i:i+10] for i in range(0,len(nums),10)]	
		summation(elements)
		# print(list(elements))
	except ValueError:
		print("Invalid Input")
		print("Provided Input: "+str(nums)+"\nINPUT SHOULD ONLY BE NUMBERS")
		array_input()
	return
		
def summation(elements):
	# print("Num:",nums)
	sum_batch = [sum(elems) for elems in elements]
	total_sum = sum(sum_batch)
	print("Sum of elements in Batches:",sum_batch,"\nTotal Sum:",total_sum)	
	return total_sum

array_input()	
