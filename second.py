def array_input():
	"""Function for feeding input array """

	nums = input("Enter the numbers of the series seperated by <space>\n")
	nums = nums.split()
	try:
		nums = [int(num) for num in nums]
		if(len(set(nums))==len(nums)):
			find_missing(nums)
		else:
			print("No number should be repeated\nProvide Input again")
			array_input()	
	except ValueError:
		print("Invalid Input")
		print("Provided Input: "+str(nums)+"\nINPUT SHOULD ONLY BE NUMBERS")
		array_input()
	return	


def find_missing(nums):
	"""Function for finding a missing number in unsorted array of progressive series"""
	
	series_min = min(nums)
	series_max = max(nums)		
	check_list = [num for num in range(series_min,series_max+1) ]
	missing_num = list(set(check_list).difference(set(nums)))

	if(len(missing_num)==0):
		missing_num=series_max+1
	elif(len(missing_num)==1):
		missing_num = missing_num[0]
	else:
		print("Input series is not valid! \nMore than one elements missing,\nTry Again ")
		array_input()
		return
	
	print("Missing Number :", missing_num)
	return missing_num


array_input()